# Running Locally

```
docker run -it --rm --mount type=bind,source="$(pwd)",target=/app -p 8019:8019 --link db mcr.microsoft.com/dotnet/core/sdk
```

```
cd /app
dotnet restore
dotnet
```