﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo.Models;
using Demo.Data;

namespace Demo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext db;

    public HomeController(ApplicationDbContext db)
    {
        this.db = db;
    }

        public IActionResult Index()
        {
            return View(db.Customers.ToList());
        }
    }
}